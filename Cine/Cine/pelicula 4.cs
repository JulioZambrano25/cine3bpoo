﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cine
{
    class pelicula_4:cartelera
    {
        private string titulo4;
        private string proyeccion;

        public string Titulo4 { get => titulo4; set => titulo4 = value; }
        public string Proyeccion { get => proyeccion; set => proyeccion = value; }

        public pelicula_4(string titulo4, string proyeccion)
        {
            this.Titulo4 = titulo4;
            this.Proyeccion = proyeccion;
        }
        public override string ToString()
        {
            return "4." + titulo4 + "   se proyecta en la  " + proyeccion;
        }
    }
}
