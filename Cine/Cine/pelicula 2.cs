﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cine
{
    class pelicula_2:cartelera
    {
        private string titulo2;
        private string proyeccion;

        public string Titulo2 { get => titulo2; set => titulo2 = value; }
        public string Proyeccion { get => proyeccion; set => proyeccion = value; }

        public pelicula_2(string titulo2, string proyeccion)
        {
            this.Titulo2 = titulo2;
            this.Proyeccion = proyeccion;
        }
        public override string ToString()
        {
            return "2. " + titulo2 + "  se proyecta en la  " + proyeccion;
        }
    }
}
