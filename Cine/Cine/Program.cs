﻿using System;

namespace Cine
{
    class Program
    {
        static void Main(string[] args)
        {
            /*PARALELO B:
            EJERCICIO PRÁCTICO #2
            CINE 2020
            Desarrollar las clases, atributos, asociaciones para generar el diagrama de clases, código e implementación(considerando la reutilización para otros cines y funciones), siendo los requerimientos:
            •	En el cine estarán disponibles varias películas: John Wick 3, Aladdin, Avengers, Pikachu, etc.
            •	Una película podrá ser presentada en varias salas según los asientos aún disponibles.
            •	Al momento de comprar una película los clientes deben seleccionar los asientos de una sala disponible para la película que desea ver.
            •	Al momento de seleccionar sus asientos, el cliente debe indicar la lista de productos comestibles que desea consumir mientras disfruta de la película. Por ejemplo: Canguil, Hot - dog, Bebidas, etc.
            •	Cada sala consta de forma general con aperitivos o accesorios que son suministrados de forma gratuita a todos sus visitantes.
            •	Se solicita como parte de la implementación un método que muestre el siguiente detalle por cada cliente en su película: nombre del cliente, película escogida, sala de cine, asiento donde se encuentra ubicado, aperitivos o accesorios suministrados en la sala de forma gratuita, Productos comestibles que consumirá el cliente mientras ve la cinta.*/
           
            
            //Aqui comienza el codigo de cine
            int s2 = 0;
            for (int i = 1; s2 > 1; i++)
            {
                Console.WriteLine("BIENVENIDO AL ¨cine Buena vista ");
                Console.WriteLine("Por favor, selecciona una opcion: " +
                "\n1.- INGRESAR:" +
                "\n2.- ESCOGER PELICULAS:" +
                "\n3.- ESCOGER ACIENTOS:" +
                "\n4.- ESCOGER COMESTIBLES:" +
                "\n5.- SALIR:\n");

                String s1 = null;
                s1 = Console.ReadLine();

                switch (s1)
                {


                    case "1":
                        Cliente objpersona = new Cliente();
                        objpersona.ponernombre("cualquier nombre");
                        Console.WriteLine("El cliente:  " + objpersona.Nombres1);
                        Cliente objcedula = new Cliente();
                        objpersona.ponercedula(11111);
                        Console.WriteLine("con el numero de cedula:" + objcedula.Cedula);


                        break;

                    case "2":
                        Console.WriteLine("AQUI ENCONTRARA LA LISTA DE PELICULAS DISPONIBLES:");
                        Console.WriteLine("Por favor, selecciona una opcion: " +
                          "\n1.- Aladinn:" +
                          "\n2.- Pikachu:" +
                          "\n3.- jhon wick 3:" +
                          "\n4.- avengers:" +
                          "\n5.- SALIR:\n");
                        String s3 = null;
                        s3 = Console.ReadLine();


                        // Se implementa un switch dentro de otro switch
                        switch (s3)
                        {
                            case "1":
                                Pelicula pelicula = new Pelicula("Aladinn", "sala a y b");
                                Console.WriteLine(pelicula.ToString());
                                Console.WriteLine("Por favor, selecciona una opcion de la sala: " +
                          "\n1.- A:" +
                          "\n2.- B:");
                                string s4 = null;
                                s4 = Console.ReadLine();
                                switch (s4)
                                {
                                    case "1":
                                        Console.WriteLine("usted a seleccionado la sala a");
                                        break;
                                    case "2":
                                        Console.WriteLine("usted a seleccionado la sala B");
                                        break;
                                }
                                break;

                            case "2":
                                pelicula_2 pelicula_2 = new pelicula_2("Pikachu", "sala c y d");
                                Console.WriteLine(pelicula_2.ToString());
                                Console.WriteLine("Por favor, selecciona una opcion de la sala: " +
                          "\n1.- c:" +
                          "\n2.- d:");
                                string s5 = null;
                                s5 = Console.ReadLine();
                                switch (s5)
                                {
                                    case "1":
                                        Console.WriteLine("usted a seleccionado la sala c");
                                        break;
                                    case "2":
                                        Console.WriteLine("usted a seleccionado la sala d");
                                        break;


                                }
                                break;
                            case "3":
                                pelicula_3 pelicula_3 = new pelicula_3("Jhon wick 3", "sala e y f");
                                Console.WriteLine(pelicula_3.ToString());
                                Console.WriteLine("Por favor, selecciona una opcion de la sala: " +
                          "\n1.- e:" +
                          "\n2.- f:");
                                string s6 = null;
                                s6 = Console.ReadLine();
                                switch (s6)
                                {
                                    case "e":
                                        Console.WriteLine("usted a seleccionado la sala e");
                                        break;
                                    case "f":
                                        Console.WriteLine("usted a seleccionado la sala f");
                                        break;
                                }
                                break;
                            case "4":
                                pelicula_4 pelicula_4 = new pelicula_4("Avengers:la era del cocolon", "sala g y h");
                                Console.WriteLine(pelicula_4.ToString());
                                Console.WriteLine("Por favor, selecciona una opcion de la sala: " +
                          "\n1.- g:" +
                          "\n2.- h:");
                                string s7 = null;
                                s7 = Console.ReadLine();
                                switch (s7)
                                {
                                    case "g":
                                        Console.WriteLine("usted a seleccionado la sala g");
                                        break;
                                    case "h":
                                        Console.WriteLine("usted a seleccionado la sala h");
                                        break;

                                }
                                break;
                        }


                        break;

                    case "3":
                        Console.WriteLine("OPCION PARA ESCOGER ACIENTOS");


                        break;

                    case "4":
                        Console.WriteLine("OPCION PARA ESCOGER COMESTIBLES");
                        

                        break;

                    case "5":
                        Console.WriteLine("Gracias vuelva pronto");
                        s2 = 1;

                        break;

                    default:
                        Console.WriteLine("No se ha seleccionado una opcion");
                        break;
                }

            }
        }
    }
}
