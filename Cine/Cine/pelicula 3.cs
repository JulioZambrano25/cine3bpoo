﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cine
{
    class pelicula_3:cartelera
    {
        private string titulo3;
        private string proyeccion;

        public string Titulo3 { get => titulo3; set => titulo3 = value; }
        public string Proyeccion { get => proyeccion; set => proyeccion = value; }

        public pelicula_3(string titulo3, string proyeccion)
        {
            this.Titulo3 = titulo3;
            this.Proyeccion = proyeccion;
        }
        public override string ToString()
        {
            return "3." + titulo3 + "   se proyecta en la" + proyeccion;
        }
    }
}
