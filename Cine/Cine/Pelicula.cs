﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cine
{
    class Pelicula:cartelera
    {
        private string titulo1;
        private string proyeccion;

        public string Titulo1 { get => titulo1; set => titulo1 = value; }
        public string Proyeccion { get => proyeccion; set => proyeccion = value; }

        public Pelicula(string titulo1, string proyeccion)
        {
            this.Titulo1 = titulo1;
            this.Proyeccion = proyeccion;
        }
        public override string ToString()
        {
            return "1. " + titulo1 + "  se proyecta en la  " + proyeccion;
        }

    }
}
